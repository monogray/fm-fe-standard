function Departments() {

    // This obj
    var thisDepartments = this;

    // Properties
    this.apiUrl = apiUrl;

    // Models
    var departmentsModel = new DepartmentsModel(this.apiUrl);
    var employeesModel = new EmployeesModel(this.apiUrl);

    this.run = function() {
        this.prepareModels();
        this.addListeners();
        this.logic();
    };


    // ---------------------------------------------
    // Prepare models
    // ---------------------------------------------

    this.prepareModels = function() {
    };


    // ---------------------------------------------
    // Listeners
    // ---------------------------------------------

    this.addListeners = function() {
        // Update department
        $( "#update-button" ).click(function() {
            // Prepare ID
            var _id = 1;

            // Prepare Data
            var _data = {"name": "Test department"};

            departmentsModel.update(_id, _data, thisDepartments.onDepartmentUpdate_success, thisDepartments.onDepartmentUpdate_error);
        });

        // Create department
        // ...

        // ...
        // ...
    };


    // ---------------------------------------------
    // Logic
    // ---------------------------------------------

    this.logic = function() {

    };

    this.onDepartmentUpdate_success = function(data) {
        console.log("onDepartmentUpdate_success");
    };

    this.onDepartmentUpdate_error = function(jqXHR, textStatus) {
        console.log("onDepartmentUpdate_error");
    };
}

var departments = new Departments();
departments.run();
