function DepartmentsModel(apiUrl) {

    this.apiUrl = apiUrl;

    this.create = function () {
    };

    this.update = function (id, data, successCallback, errorCallback) {
        // Prepare id
        id = parseInt(id);

        // Prepare data
        // ...

        $.ajax({
                method: "PUT",
                url: apiUrl + "/departments/" + id,
                data: data
            })
            .done(function (response) {
                // Prepare response
                // ...

                // Callback
                successCallback(response);
            })
            .fail(function (jqXHR, textStatus) {

                // Callback
                errorCallback(jqXHR, textStatus);
            });
    };

    this.get = function (id) {
    };

    this.getList = function (page) {
    };

    this.hide = function (id) {
    };
}

function EmployeesModel(apiUrl) {

    this.apiUrl = apiUrl;

    this.create = function () {
    };

    this.update = function (id, data, successCallback, errorCallback) {
    };

    this.get = function (id) {
    };

    this.getList = function (page) {
    };

    this.hide = function (id) {
    };
}


